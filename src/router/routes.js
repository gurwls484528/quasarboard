const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      {
        path: "/posts",
        component: () => import("pages/posts/PostListView.vue"),
      },
      {
        path: "/edit/:id",
        component: () => import("pages/posts/PostEditView.vue"),
      },
      {
        path: "/create",
        component: () => import("pages/posts/PostCreateView.vue"),
      },
      {
        path: "/detail/:id",
        component: () => import("pages/posts/PostDetailView.vue"),
        props: true,
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
