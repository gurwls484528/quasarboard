import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "../src/router";
// import focus from '@/directives/focus';
// import globalComponent from '@/plugins/global-component';

const app = createApp(App);
// app.use(globalComponent);
// app.directive('focus', focus);
app.use(router);
app.use(createPinia());
app.mount("#app");
